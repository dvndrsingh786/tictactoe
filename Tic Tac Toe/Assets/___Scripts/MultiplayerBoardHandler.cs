﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Advertisements;
using Photon.Realtime;
using Photon.Pun;
using System.Collections;

public class MultiplayerBoardHandler : MonoBehaviourPunCallbacks
{

    public Sprite[] theTwoImages;
    [SerializeField] Image[] children;
    int player_turn;
    int totalTurns;
    [SerializeField] GameObject winLine;
    [SerializeField] GameObject gamePanel;
    [SerializeField] GameObject playerOneInfo, playerTwoInfo;
    [SerializeField] GameObject youWinText;
    [SerializeField] TextMeshProUGUI codeText;
    [SerializeField] PhotonView view;
    public bool myTurn = false;

    void Start()
    {
        codeText.text = PlayerPrefs.GetInt("Code").ToString();
        if (PhotonNetwork.IsMasterClient)
        {
            myTurn = true;
            player_turn = 0;
        }
        else
        {
            myTurn = false;
            player_turn = 1;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ResetGame();
            OpenMainMenu();
        }
    }

    [PunRPC]
    void MainFunction(int imageId,int spriteId)
    {
        transform.GetChild(imageId).GetComponent<MultiplayerTurn_Tapped>().ChangeSprite(spriteId);
        myTurn = true;
    }

    [PunRPC]
    void YouLost(Vector3 winLinePos, Vector3 winLineRot)
    {
        winLine.transform.localEulerAngles = winLineRot;
        winLine.transform.localPosition = winLinePos;
        winLine.SetActive(true);
        youWinText.GetComponent<TextMeshProUGUI>().text = "You Lost";
        youWinText.SetActive(true);
    }

    public void Image_Clicked(int id)
    {
        if (!transform.GetChild(id).GetComponent<Image>().sprite && !winLine.activeInHierarchy)
        {
            //if (player_turn == 0) player_turn = 1;
            //else player_turn = 0;
            transform.GetChild(id).GetComponent<Image>().sprite = theTwoImages[player_turn];
            transform.GetChild(id).GetComponent<Image>().color += new Color(0, 0, 0, 1);
            transform.GetChild(id).GetComponent<MultiplayerTurn_Tapped>().spriteId = player_turn;
            myTurn = false;
            view.RPC("MainFunction", RpcTarget.OthersBuffered, id, player_turn);
            totalTurns++;
            if (totalTurns > 2) CheckCombo();
        }
    }

    void CheckCombo()
    {
        bool win = false;
        if (children[0].sprite && children[1].sprite && children[2].sprite)
        {
            if (children[0].sprite.name == children[1].sprite.name && children[1].sprite.name == children[2].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, 90);
                winLine.transform.localPosition += new Vector3(0, 380, 0);
                win = true;
            }

        }
        if (children[3].sprite && children[4].sprite && children[5].sprite)
        {
            if (children[3].sprite.name == children[4].sprite.name && children[4].sprite.name == children[5].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, 90);
                winLine.transform.localPosition += new Vector3(0, 0, 0);
                win = true;
            }
        }
        if (children[6].sprite && children[7].sprite && children[8].sprite)
        {
            if (children[6].sprite.name == children[7].sprite.name && children[7].sprite.name == children[8].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, 90);
                winLine.transform.localPosition += new Vector3(0, -390, 0);
                win = true;
            }
        }
        if (children[0].sprite && children[3].sprite && children[6].sprite)
        {
            if (children[0].sprite?.name == children[3].sprite.name && children[3].sprite.name == children[6].sprite.name)
            {
                winLine.transform.localPosition += new Vector3(-380, 0, 0);
                win = true;
            }
        }
        if (children[1].sprite && children[4].sprite && children[7].sprite)
        {
            if (children[1].sprite.name == children[4].sprite.name && children[4].sprite.name == children[7].sprite.name)
            {
                winLine.transform.localPosition += new Vector3(-10, 0, 0);
                win = true;
            }
        }
        if (children[2].sprite && children[5].sprite && children[8].sprite)
        {
            if (children[2].sprite.name == children[5].sprite.name && children[5].sprite.name == children[8].sprite.name)
            {
                winLine.transform.localPosition += new Vector3(350, 0, 0);
                win = true;
            }

        }
        if (children[0].sprite && children[4].sprite && children[8].sprite)
        {
            if (children[0].sprite.name == children[4].sprite.name && children[4].sprite.name == children[8].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, 44);
                winLine.transform.localPosition += new Vector3(0, 0, 0);
                win = true;
            }
        }
        if (children[2].sprite && children[4].sprite && children[6].sprite)
        {
            if (children[2].sprite.name == children[4].sprite.name && children[4].sprite.name == children[6].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, -43.5f);
                winLine.transform.localPosition += new Vector3(-22, 0, 0);
                win = true;
            }
        }
        if (win)
        {
            winLine.SetActive(true);
            youWinText.GetComponent<TextMeshProUGUI>().text = "You Win";
            youWinText.SetActive(true);
            view.RPC("YouLost", RpcTarget.Others, winLine.transform.localPosition, winLine.transform.localEulerAngles);
            Vibration.Vibrate(150);
        }
        else if (!win && totalTurns == 5)
        {
            youWinText.GetComponent<TextMeshProUGUI>().text = "Game Tied";
            youWinText.SetActive(true);
            view.RPC("GameTied", RpcTarget.Others);
            Vibration.Vibrate(150);
        }
    }

    [PunRPC]
    void GameTied()
    {
        youWinText.GetComponent<TextMeshProUGUI>().text = "Game Tied";
        youWinText.SetActive(true);
        Vibration.Vibrate(150);
    }

    public void ResetAll()
    {
        view.RPC("OnlineReset", RpcTarget.All);
    }

    void ResetGame()
    {
        for (int i = 0; i < 9; i++)
        {
            children[i].sprite = null;
            children[i].color = new Color(children[i].color.r, children[i].color.g, children[i].color.b, 0);
            children[i].GetComponent<MultiplayerTurn_Tapped>().spriteId = -1;
        }
        winLine.transform.localEulerAngles = Vector3.zero;
        winLine.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        winLine.SetActive(false);
        if (PhotonNetwork.IsMasterClient)
        {
            player_turn = 0;
            myTurn = true;
        }
        else
        {
            player_turn = 1;
            myTurn = false;
        }
        totalTurns = 0;
        youWinText.SetActive(false);
    }

    [PunRPC]
    void OnlineReset()
    {
        ResetGame();
    }

    public void OpenMainMenu()
    {
        PhotonNetwork.Disconnect();
        UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        PhotonNetwork.Disconnect();
        UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
    }

    public Texture2D screenTex;
    NativeShare nativeShare;
    string body = "";
    string body1 = "Play Tic Tac Toe Game with me, I am waiting for you in room. Enter Code \"";
    string body2 = "\"and click join to start playing with me";

    void FinalShare(Texture2D tex)
    {
        nativeShare.AddFile(tex).SetText(body).Share();
    }

    public void Share()
    {
        StartCoroutine(Test());
    }

    IEnumerator Test()
    {
        yield return new WaitForEndOfFrame();
        screenTex = ScreenCapture.CaptureScreenshotAsTexture();
        body = body1 + PlayerPrefs.GetInt("Code") + body2;
        FinalShare(screenTex);
    }
}

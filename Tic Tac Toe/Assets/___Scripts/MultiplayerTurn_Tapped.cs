﻿using UnityEngine;
using UnityEngine.EventSystems;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine.UI;

public class MultiplayerTurn_Tapped : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] int image_id;
    public int spriteId;
    [SerializeField] MultiplayerBoardHandler boardHndlr;

    public void ChangeSprite(int spriteIDd)
    {
        spriteId = spriteIDd;
        GetComponent<Image>().sprite = boardHndlr.theTwoImages[spriteId];
        GetComponent<Image>().color += new Color(0, 0, 0, 1);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (boardHndlr.myTurn)
        {
            boardHndlr.Image_Clicked(image_id);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System;
using UnityEngine.SceneManagement;
using TMPro;

public class MultiplayerHandler : MonoBehaviourPunCallbacks
{

    bool createRoom = false;
    int createRoomCode;
    [SerializeField] TMP_InputField joinRoomCode;
    RoomOptions opts;

    private void Start()
    {
        opts = new RoomOptions();
        opts.MaxPlayers = 2;
    }

    public void ConnectToMaster(bool create)
    {
        if(PhotonNetwork.InRoom) PhotonNetwork.LeaveRoom();
        createRoom = create;
        if (PhotonNetwork.IsConnected) PhotonNetwork.Disconnect();
        PhotonNetwork.ConnectUsingSettings();
    }

    void JoinRoom()
    {
        PlayerPrefs.SetInt("Code", int.Parse(joinRoomCode.text));
        PhotonNetwork.JoinRoom(joinRoomCode.text.ToString());
    }

    void CreateRoom()
    {
        createRoomCode = UnityEngine.Random.Range(0, 999999);
        PlayerPrefs.SetInt("Code", createRoomCode);
        PhotonNetwork.CreateRoom(createRoomCode.ToString(), opts, TypedLobby.Default);
    }

    public override void OnConnectedToMaster()
    {
        if (createRoom) CreateRoom();
        else JoinRoom();
    }

    public override void OnCreatedRoom()
    {
        Debug.LogError("Room Created");
    }

    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("Multiplayer Game Scene");
        SceneManager.activeSceneChanged += SceneChanged;
    }

    public void SceneChanged(Scene currentScene, Scene newScene)
    {
        if (currentScene != null) Debug.LogError(currentScene.name);
        Debug.LogError(newScene.name);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.LogError("Room Creation Failed");
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.LogError("Join Room Failed");
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.LogError("Player Left Room");
    }

    public override void OnCustomAuthenticationFailed(string debugMessage)
    {
        Debug.LogError("Custom Authentication Failed");
    }
}

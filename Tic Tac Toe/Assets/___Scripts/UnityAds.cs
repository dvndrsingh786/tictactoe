﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;


public class UnityAds : MonoBehaviour, IUnityAdsListener
{
    string placementId = "rewardedVideo";
    string gameId = "3497496";

    // Start is called before the first frame update
    void Start()
    {
        Advertisement.Initialize(gameId, false);
        if (!Advertisement.isInitialized)
        {
            Initialize();
        }
        else
        {
            Debug.LogError("Initialized");
        }
        //ShowRewardedAd();
        Advertisement.AddListener(this);
        
    }
    int mainInitialize = 0;
    void Initialize()
    {
        if (!Advertisement.isInitialized && mainInitialize < 2500)
        {
            mainInitialize++;
            Invoke(nameof(Initialize), 0.01f);
        }
        else
        {
            mainInitialize = 0;
        }
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady(placementId))
        {
            Advertisement.Show(placementId);
        }
        else
        {
            Invoke(nameof(ShowRewardedAd), 0.01f);
        }
    }

    int videoInitialize = 0;
    public void ShowBasicAd()
    {
        if (Advertisement.IsReady("video"))
        {
            videoInitialize = 0;
            Advertisement.Show("video");
        }
        else if(videoInitialize < 2500)
        {
            videoInitialize++;
            Invoke(nameof(ShowBasicAd), 0.01f);
        }
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {

        }
        else if (showResult == ShowResult.Skipped)
        {

        }
        else if (showResult == ShowResult.Failed)
        {

        }
    }

    public void OnUnityAdsReady(string placementId)
    {

    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.LogError(message);
    }

    public void OnUnityAdsDidStart(string placementId)
    {

    }
}

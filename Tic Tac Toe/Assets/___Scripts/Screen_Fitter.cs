﻿using UnityEngine;

public class Screen_Fitter : MonoBehaviour
{
    [SerializeField] RectTransform canvas;
    [Header("Fit On Screen")]
    [SerializeField] bool screenFit;
    [Header("Fit With Percentage")]
    [SerializeField] bool fitWithPercentage;
    [SerializeField] int widthPercentage, heightPercentage;
    [Header("Fit At Constraint")]
    [SerializeField] bool width;
    [SerializeField] bool height;



    // Start is called before the first frame update
    void Start()
    {
        if (screenFit)
        {
            transform.GetComponent<RectTransform>().sizeDelta = canvas.sizeDelta;
        }
        else if(fitWithPercentage)
        {
            transform.GetComponent<RectTransform>().sizeDelta = new Vector2(canvas.sizeDelta.x * ((float)widthPercentage / 100), canvas.sizeDelta.y * ((float)heightPercentage / 100));
        }
        else
        {
            if (width)
            {
                transform.GetComponent<RectTransform>().sizeDelta = new Vector2(canvas.sizeDelta.x, transform.GetComponent<RectTransform>().sizeDelta.y);
            }
            if (height)
            {
                transform.GetComponent<RectTransform>().sizeDelta = new Vector2(transform.GetComponent<RectTransform>().sizeDelta.x, canvas.sizeDelta.y);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

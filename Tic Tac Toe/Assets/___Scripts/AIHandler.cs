﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIHandler : MonoBehaviour
{
    [SerializeField] int[] codeTray = new int[9];
    [SerializeField] BoardHandler boardHndlr;

    public void ResetArray()
    {
        for (int i = 0; i < 9; i++)
        {
            codeTray[i] = 2;
        }
    }

    public void AddPlayerTurn(int code)
    {
        codeTray[code] = 0;
    }
    public void AddAITurn(int code)
    {
        codeTray[code] = 1;
    }

     public IEnumerator WaitBeforeChecking()
    {
        yield return new WaitForSeconds(1);
        CheckAll(1);
    }

    public void CheckAll(int type)
    {
        bool found = false;
        #region Horizontal 1
        if (codeTray[0] == codeTray[1] && codeTray[2] == 2 && !found && codeTray[0] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(2);
        }
        if (codeTray[1] == codeTray[2] && codeTray[0] == 2 && !found && codeTray[1] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(0);

        }
        if (codeTray[0] == codeTray[2] && codeTray[1] == 2 && !found && codeTray[0] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(1);

        }
        #endregion
        #region Vertical 1
        if (codeTray[0] == codeTray[3] && codeTray[6] == 2 && !found && codeTray[0] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(6);

        }
        if (codeTray[3] == codeTray[6] && codeTray[0] == 2 && !found && codeTray[3] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(0);

        }
        if (codeTray[0] == codeTray[6] && codeTray[3] == 2 && !found && codeTray[0] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(3);

        }
        #endregion
        #region Diagonal 1
        if (codeTray[0] == codeTray[4] && codeTray[8] == 2 && !found && codeTray[0] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(8);

        }
        if (codeTray[4] == codeTray[8] && codeTray[0] == 2 && !found && codeTray[4] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(0);

        }
        if (codeTray[0] == codeTray[8] && codeTray[4] == 2 && !found && codeTray[0] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(4);

        }
        #endregion
        #region Vertical 2
        if (codeTray[1] == codeTray[4] && codeTray[7] == 2 && !found && codeTray[1] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(7);

        }
        if (codeTray[4] == codeTray[7] && codeTray[1] == 2 && !found && codeTray[4] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(1);

        }
        if (codeTray[1] == codeTray[7] && codeTray[4] == 2 && !found && codeTray[1] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(4);

        }
        #endregion
        #region Diagonal 2
        if (codeTray[2] == codeTray[4] && codeTray[6] == 2 && !found && codeTray[2] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(6);

        }
        if (codeTray[4] == codeTray[6] && codeTray[2] == 2 && !found && codeTray[4] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(2);

        }
        if (codeTray[2] == codeTray[6] && codeTray[4] == 2 && !found && codeTray[2] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(4);

        }
        #endregion
        #region Vertical 3
        if (codeTray[2] == codeTray[5] && codeTray[8] == 2 && !found && codeTray[2] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(8);

        }
        if (codeTray[5] == codeTray[8] && codeTray[2] == 2 && !found && codeTray[5] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(2);

        }
        if (codeTray[2] == codeTray[8] && codeTray[5] == 2 && !found && codeTray[2] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(5);

        }
        #endregion
        #region Horizontal 2
        if (codeTray[3] == codeTray[4] && codeTray[5] == 2 && !found && codeTray[3] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(5);

        }
        if (codeTray[4] == codeTray[5] && codeTray[3] == 2 && !found && codeTray[4] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(3);

        }
        if (codeTray[3] == codeTray[5] && codeTray[4] == 2 && !found && codeTray[3] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(4);

        }
        #endregion
        #region Horizontal 3
        if (codeTray[6] == codeTray[7] && codeTray[8] == 2 && !found && codeTray[6] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(8);

        }
        if (codeTray[7] == codeTray[8] && codeTray[6] == 2 && !found && codeTray[7] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(6);

        }
        if (codeTray[6] == codeTray[8] && codeTray[7] == 2 && !found && codeTray[6] == type)
        {
            found = true;
            boardHndlr.AiImageClicked(7);

        }
        #endregion

        if(type == 1 && !found)
        {
            CheckAll(0);
        }
        else if(type == 0 && !found)
        {
            //for (int i = 0; i < 9; i++)
            //{
            //    if (codeTray[i] == 2)
            //    {
            //        boardHndlr.AiImageClicked(i);
            //        return;
            //    }
            //}
            int i = 0;
            do
            {
                i = Random.Range(0, 9);
            }
            while (codeTray[i] != 2);
            boardHndlr.AiImageClicked(i);
        }
    }
}

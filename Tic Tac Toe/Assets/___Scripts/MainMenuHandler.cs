﻿using UnityEngine;
using TMPro;

public class MainMenuHandler : MonoBehaviour
{

    public GameObject settingsPanel;
    [SerializeField] TMP_InputField player1IP, player2IP;
    [SerializeField] GameObject gamePanel;
    [SerializeField] SettingsHandler sttngsHndlr;
    [SerializeField] ThemePanelHndlr themepnlHndlr;
    [SerializeField] GameObject themePanel;
    [SerializeField] AudioSource buttonMusic;
    [SerializeField] GameObject exitPAnel;
    public GameObject uploadScorePanel;
    [SerializeField] StatManager statMngr;
    [SerializeField] GameObject statsMngrGO;
    

    void Start()
    {
        CheckPlayerPrefes();
        UsePlayerPrefs();
        player1IP.characterLimit = 10;
        player2IP.characterLimit = 10;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(!themePanel.activeInHierarchy && !settingsPanel.activeInHierarchy && !uploadScorePanel.activeInHierarchy) ExitPanelSwitch();
            if (settingsPanel.activeInHierarchy)
            {
                settingsPanel.SetActive(false);
            }
            else if (themePanel.activeInHierarchy)
            {
                themePanel.SetActive(false);
            }
            else if (uploadScorePanel.activeInHierarchy)
            {
                uploadScorePanel.SetActive(false);
            }
        }
    }

    void CheckPlayerPrefes()
    {
        if (!PlayerPrefs.HasKey("PlayerOne"))
        {
            PlayerPrefs.SetString("PlayerOne", "Unknown");
            PlayerPrefs.SetString("PlayerTwo", "Unknown");
            PlayerPrefs.SetFloat("Volume", 1);
            PlayerPrefs.SetString("Vibration", "True");
            PlayerPrefs.SetInt("PlayerOneNumber", 0);
            PlayerPrefs.SetInt("PlayerTwoNumber", 1);
            PlayerPrefs.SetInt("Theme", 0);
            PlayerPrefs.SetInt("TotalGames", 0);
        }
    }

    void UsePlayerPrefs()
    {
        if (PlayerPrefs.GetString("PlayerOne") != "Unknown")
        {
            player1IP.text = PlayerPrefs.GetString("PlayerOne");
        }
        if (PlayerPrefs.GetString("PlayerTwo") != "Unknown")
        {
            player2IP.text = PlayerPrefs.GetString("PlayerTwo");
        }
        sttngsHndlr.CancelSettings();
        themepnlHndlr.SetPlayerPrefs();
    }

    public void SettingsPanelToggle()
    {
        if (!settingsPanel.activeInHierarchy)
        {
            if (uploadScorePanel.activeInHierarchy) uploadScorePanel.SetActive(false);
            settingsPanel.SetActive(true);
        }
        else
        {
            sttngsHndlr.CancelSettings();
        }
    }
    public void Player1SetPP()
    {
        PlayerPrefs.SetString("PlayerOne", player1IP.text);
    }
    public void Player2SetPP()
    {
        PlayerPrefs.SetString("PlayerTwo", player2IP.text);
    }

    public void OpenThemePanel()
    {
        themePanel.SetActive(true);
        if (settingsPanel.activeInHierarchy)
        {
            sttngsHndlr.CancelSettings();
        }
    }



    public void ButtonClickSound()
    {
        buttonMusic.Play();
    }

    public void StartGame()
    {
        gamePanel.SetActive(true);
        gameObject.SetActive(false);
    }

    public void ExitPanelSwitch()
    {
        if (exitPAnel.activeInHierarchy)
        {
            exitPAnel.SetActive(false);
        }
        else
        {
            exitPAnel.SetActive(true);
        }
    }

    public void StatManagerToggle()
    {
        if (!statsMngrGO.activeInHierarchy)
        {
            statMngr.Sort();
            statMngr.SetAll();
            statsMngrGO.SetActive(true);
        }
        else
        {
            statsMngrGO.SetActive(false);
        }
    }

    //public void UploadPanelClose()
    //{
    //    uploadScorePanel.SetActive(false);
    //}

    //public void UploadScore()
    //{
    //    if (settingsPanel.activeInHierarchy)
    //    {
    //        sttngsHndlr.CancelSettings();
    //    }
    //    PlayGamesScript.instance.AddScoreToLEaderBoard(GPGSIds.leaderboard_most_games, PlayerPrefs.GetInt("TotalGames"));
    //}

    //public void ShowLEaderboard()
    //{
    //    PlayGamesScript.instance.ShowLEaderBoardUI();
    //}

    public void ExitGame()
    {
        Application.Quit();
    }

    public void LikeButtonClick()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.DefaultCompany.TicTacToeZeroKanta");
    }

    public void Instagram()
    {
        Application.OpenURL("https://www.instagram.com/wahoplay");
    }

    public void Facebook()
    {
        Application.OpenURL("https://www.Facebook.com/wahoplay");
    }
}

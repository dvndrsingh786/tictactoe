﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoHider : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke(nameof(Hide), 2);
    }


    void Hide()
    {
        gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

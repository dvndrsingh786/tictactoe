﻿using UnityEngine;
using UnityEngine.UI;

public class ThemePanelHndlr : MonoBehaviour
{
    int selectedTheme = 0;
    [SerializeField] BoardHandler boardHndlr;
    [SerializeField] Color[] colorThemes;
    [SerializeField] Image[] theObjects;
    [SerializeField] SettingsHandler sttngsHndlr;

    public void SetPlayerPrefs()
    {
        selectedTheme = PlayerPrefs.GetInt("Theme");
        for (int i = 0; i < 4; i++)
        {
            theObjects[i].color = colorThemes[selectedTheme];
        }
        boardHndlr.SetBackGround(colorThemes[selectedTheme]);
        sttngsHndlr.SetSignColor(colorThemes[selectedTheme]);
    }

    public void ChangeSelectedTheme(bool increase)
    {
        if (increase)
        {
            if (selectedTheme < 5)
            {
                selectedTheme++;
                for (int i = 0; i < 4; i++)
                {
                    theObjects[i].color = colorThemes[selectedTheme];
                }

            }
        }
        else
        {
            if (selectedTheme > 0)
            {
                selectedTheme--;
                for (int i = 0; i < 4; i++)
                {
                    theObjects[i].color = colorThemes[selectedTheme];
                }
            }
        }
    }

    public void ThemePanelOkay()
    {
        PlayerPrefs.SetInt("Theme", selectedTheme);
        boardHndlr.SetBackGround(colorThemes[selectedTheme]);
        sttngsHndlr.SetSignColor(colorThemes[selectedTheme]);
        gameObject.SetActive(false);
    }
}

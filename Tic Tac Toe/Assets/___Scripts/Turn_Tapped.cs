﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Turn_Tapped : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] int image_id;
    [SerializeField] BoardHandler boardHndlr;

    public void OnPointerClick(PointerEventData eventData)
    {   
        boardHndlr.Image_Clicked(image_id);
    }
}

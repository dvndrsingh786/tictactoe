﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsHandler : MonoBehaviour
{
    [SerializeField] AudioSource buttonMusic;
    [SerializeField] Slider volumeSlider;
    [SerializeField] Toggle vibrationToggle;
    [SerializeField] Sprite[] playerSignsSprites;
    [SerializeField]int playerOneNumber, playerTwoNumber;
    public Image playerOneImage, playerTwoImage;
    public Color emojiColor;
    public ColorUsageAttribute ssdsd;

    public void SaveSettings()
    {
        PlayerPrefs.SetFloat("Volume", volumeSlider.value / 100);
        buttonMusic.volume = PlayerPrefs.GetFloat("Volume");
        if (vibrationToggle.isOn)
        {
            PlayerPrefs.SetString("Vibration", "True");
        }
        else
        {
            PlayerPrefs.SetString("Vibration", "False");
        }
        PlayerPrefs.SetInt("PlayerOneNumber", playerOneNumber);
        PlayerPrefs.SetInt("PlayerTwoNumber", playerTwoNumber);
        gameObject.SetActive(false);
    }

    public void CancelSettings()
    {
        volumeSlider.value = PlayerPrefs.GetFloat("Volume") * 100;
        buttonMusic.volume = PlayerPrefs.GetFloat("Volume");
        if (PlayerPrefs.GetString("Vibration") == "True")
        {
            vibrationToggle.isOn = true;
        }
        else
        {
            vibrationToggle.isOn = false;
        }
        playerOneNumber = PlayerPrefs.GetInt("PlayerOneNumber");
        playerTwoNumber = PlayerPrefs.GetInt("PlayerTwoNumber");
        playerOneImage.sprite = playerSignsSprites[playerOneNumber];
        playerTwoImage.sprite = playerSignsSprites[playerTwoNumber];
        gameObject.SetActive(false);
    }

    public void LiveVolumeChange()
    {
        buttonMusic.volume = volumeSlider.value / 100;
    }

    public void SetSignColor(Color theColor)
    {
        playerOneImage.color = theColor;
        playerTwoImage.color = theColor;
    }

    #region Signs
    public void PlayerOneChange(bool increase)
    {
        if (increase)
        {
            if (playerOneNumber < 7)
            {
                playerOneNumber++;
            }
            if (playerOneNumber == playerTwoNumber)
            {
                playerOneNumber++;
                if (playerOneNumber == 8)
                {
                    playerOneNumber = 6;
                }
            }
        }
        else
        {
            if (playerOneNumber > 0)
            {
                playerOneNumber--;
            }
            if (playerOneNumber == playerTwoNumber)
            {
                playerOneNumber--;
                if (playerOneNumber == -1)
                {
                    playerOneNumber = 1;
                }
            }
        }
        playerOneImage.sprite = playerSignsSprites[playerOneNumber];
    }

    public void PlayerTwoChange(bool increase)
    {
        if (increase)
        {
            if (playerTwoNumber < 7)
            {
                playerTwoNumber++;
            }
            if (playerTwoNumber == playerOneNumber)
            {
                playerTwoNumber++;
                if (playerTwoNumber == 8)
                {
                    playerTwoNumber = 6;
                }
            }
        }
        else
        {
            if (playerTwoNumber > 0)
            {
                playerTwoNumber--;
            }
            if (playerTwoNumber == playerOneNumber)
            {
                playerTwoNumber--;
                if (playerTwoNumber == -1)
                {
                    playerTwoNumber = 1;
                }
            }
        }
        playerTwoImage.sprite = playerSignsSprites[playerTwoNumber];
    }
    #endregion
}

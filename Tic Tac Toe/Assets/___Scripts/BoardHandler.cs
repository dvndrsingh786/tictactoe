﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Advertisements;

public class BoardHandler : MonoBehaviour
{

    [SerializeField] Sprite[] theTwoImages;
    [SerializeField] Image[] children;
    int player_turn;
    int totalTurns;
    [SerializeField] GameObject winLine;
    [SerializeField] GameObject gamePanel, mainMenuPanel;
    [SerializeField] GameObject playerOneInfo, playerTwoInfo;
    [SerializeField] SettingsHandler sttngsHndlr;
    [SerializeField] GameObject youWinText;
    [SerializeField] UnityAds Ad;
    [SerializeField] AIHandler aiHndlr;
    [SerializeField] StatManager statMngr;
    bool done = true;
    public bool theSinglePlayer = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //if (!Advertisement.isShowing)
            //{
                ResetGame();
                OpenMainMenu();
            //}
        }
    }

    public void SetGameType(bool singlePlayer)
    {
        theSinglePlayer = singlePlayer;
    }


    public void SetBackGround(Color theColor)
    {
        for (int i = 0; i < 9; i++)
        {
            transform.GetChild(i).GetComponent<Image>().color = theColor;
            transform.GetChild(i).GetComponent<Image>().color = new Color(transform.GetChild(i).GetComponent<Image>().color.r, transform.GetChild(i).GetComponent<Image>().color.g, transform.GetChild(i).GetComponent<Image>().color.b, 0);
            transform.GetComponent<Image>().color = theColor;
            playerOneInfo.transform.GetChild(0).GetComponent<Image>().color = theColor;
            playerTwoInfo.transform.GetChild(0).GetComponent<Image>().color = theColor;
        }
    }

    private void OnEnable()
    {
        theTwoImages[1] = sttngsHndlr.playerOneImage.sprite;
        theTwoImages[0] = sttngsHndlr.playerTwoImage.sprite;
        playerOneInfo.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("PlayerOne");
        if(!theSinglePlayer) playerTwoInfo.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("PlayerTwo");
        else playerTwoInfo.GetComponent<TextMeshProUGUI>().text = "Computer";
        playerOneInfo.transform.GetChild(0).GetComponent<Image>().sprite = theTwoImages[1];
        playerTwoInfo.transform.GetChild(0).GetComponent<Image>().sprite = theTwoImages[0];
        totalTurns = 0;
    }

    public void Image_Clicked(int id)
    {
        if (!transform.GetChild(id).GetComponent<Image>().sprite && !winLine.activeInHierarchy)
        {
            if (done || !theSinglePlayer)
            {
                done = false;
                if (player_turn == 0) player_turn = 1;
                else player_turn = 0;
                transform.GetChild(id).GetComponent<Image>().sprite = theTwoImages[player_turn];
                transform.GetChild(id).GetComponent<Image>().color += new Color(0, 0, 0, 1);
                totalTurns++;
                aiHndlr.AddPlayerTurn(id);
                if (totalTurns > 4) CheckCombo(true);
                else
                {
                    if (theSinglePlayer)
                    {
                        StartCoroutine(aiHndlr.WaitBeforeChecking());
                    }
                }
            }
        }
    }

    public void AiImageClicked(int id)
    {
        if (!transform.GetChild(id).GetComponent<Image>().sprite && !winLine.activeInHierarchy)
        {
            if (player_turn == 0) player_turn = 1;
            else player_turn = 0;
            transform.GetChild(id).GetComponent<Image>().sprite = theTwoImages[player_turn];
            transform.GetChild(id).GetComponent<Image>().color += new Color(0, 0, 0, 1);
            totalTurns++;
            aiHndlr.AddAITurn(id);
            if (totalTurns > 4) CheckCombo(false);
            done = true;
        }
    }

    void CheckCombo(bool canCheck)
    {
        bool win = false;
        if (children[0].sprite && children[1].sprite && children[2].sprite)
        {
            if (children[0].sprite.name == children[1].sprite.name && children[1].sprite.name == children[2].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, 90);
                winLine.transform.localPosition += new Vector3(0, 380, 0);
                win = true;
            }

        }
        if (children[3].sprite && children[4].sprite && children[5].sprite)
        {
            if (children[3].sprite.name == children[4].sprite.name && children[4].sprite.name == children[5].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, 90);
                winLine.transform.localPosition += new Vector3(0, 0, 0);
                win = true;
            }
        }
        if (children[6].sprite && children[7].sprite && children[8].sprite)
        {
            if (children[6].sprite.name == children[7].sprite.name && children[7].sprite.name == children[8].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, 90);
                winLine.transform.localPosition += new Vector3(0, -390, 0);
                win = true;
            }
        }
        if (children[0].sprite && children[3].sprite && children[6].sprite)
        {
            if (children[0].sprite?.name == children[3].sprite.name && children[3].sprite.name == children[6].sprite.name)
            {
                winLine.transform.localPosition += new Vector3(-380, 0, 0);
                win = true;
            }
        }
        if (children[1].sprite && children[4].sprite && children[7].sprite)
        {
            if (children[1].sprite.name == children[4].sprite.name && children[4].sprite.name == children[7].sprite.name)
            {
                winLine.transform.localPosition += new Vector3(-10, 0, 0);
                win = true;
            }
        }
        if (children[2].sprite && children[5].sprite && children[8].sprite)
        {
            if (children[2].sprite.name == children[5].sprite.name && children[5].sprite.name == children[8].sprite.name)
            {
                winLine.transform.localPosition += new Vector3(350, 0, 0);
                win = true;
            }

        }
        if (children[0].sprite && children[4].sprite && children[8].sprite)
        {
            if (children[0].sprite.name == children[4].sprite.name && children[4].sprite.name == children[8].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, 44);
                winLine.transform.localPosition += new Vector3(0, 0, 0);
                win = true;
            }
        }
        if (children[2].sprite && children[4].sprite && children[6].sprite)
        {
            if (children[2].sprite.name == children[4].sprite.name && children[4].sprite.name == children[6].sprite.name)
            {
                winLine.transform.localEulerAngles = new Vector3(0, 0, -43.5f);
                winLine.transform.localPosition += new Vector3(-22, 0, 0);
                win = true;
            }
        }
        if (win)
        {
            PlayerPrefs.SetInt("TotalGames", PlayerPrefs.GetInt("TotalGames") + 1);
            winLine.SetActive(true);
            if (totalTurns % 2 == 0)
            {
                if (!theSinglePlayer)
                {
                    youWinText.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("PlayerTwo") + " Win";
                    statMngr.MultiplayerAddWin(PlayerPrefs.GetString("PlayerTwo"));
                }
                else
                {
                    youWinText.GetComponent<TextMeshProUGUI>().text = "Computer Win";
                    statMngr.ComputerWin();
                }
            }
            else
            {
                youWinText.GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetString("PlayerOne") + " Win";
                if (!theSinglePlayer) statMngr.MultiplayerAddWin(PlayerPrefs.GetString("PlayerOne"));
                else statMngr.SinglePlayerWin();
            }
            youWinText.SetActive(true);
            Vibration.Vibrate(150);
            canCheck = false;
            if (PlayerPrefs.GetInt("TotalGames") % 4 == 0)
            {
                Ad.ShowBasicAd();
            }
            Debug.LogError(PlayerPrefs.GetInt("TotalGames"));
        }
        else if(!win && totalTurns == 9)
        {
            PlayerPrefs.SetInt("TotalGames", PlayerPrefs.GetInt("TotalGames") + 1);
            Vibration.Vibrate(150);
            youWinText.SetActive(true);
            youWinText.GetComponent<TextMeshProUGUI>().text = "Game Tied";
            if (PlayerPrefs.GetInt("TotalGames") % 4  == 0)
            {
                Ad.ShowBasicAd();
            }
            Debug.LogError(PlayerPrefs.GetInt("TotalGames"));
            canCheck = false;
        }
        if (canCheck)
        {
            if (theSinglePlayer)
            {
                StartCoroutine(aiHndlr.WaitBeforeChecking());
            }
        }
    }


    public void ResetGame()
    {
        for (int i = 0; i < 9; i++)
        {
            children[i].sprite = null;
            children[i].color = new Color(children[i].color.r, children[i].color.g, children[i].color.b, 0);
        }
        winLine.transform.localEulerAngles = Vector3.zero;
        winLine.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        winLine.SetActive(false);
        player_turn = 0;
        totalTurns = 0;
        youWinText.SetActive(false);
        aiHndlr.ResetArray();
        done = true;
    }

    public void OpenMainMenu()
    {
        gamePanel.SetActive(false);
        mainMenuPanel.SetActive(true);
        youWinText.SetActive(false);
    }
}

﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StatManager : MonoBehaviour
{

    public Stats stats;
    readonly string keySats = "keyStats";
    public TextMeshProUGUI[] multiplayerNames, multiplayerScores;
    public TextMeshProUGUI singlePlayerScore, computerScore;
    public GameObject pvpPanel, pvcPanel;
    [SerializeField] Button PVPButton, PVCButton;


    void Start()
    {
        if (!PlayerPrefs.HasKey(keySats))
        {
            stats = new Stats();
            stats.theWins = new Wins();
            stats.name = new List<string>();
            stats.theWins.wins = new List<int>();
            PlayerPrefs.SetString(keySats, JsonUtility.ToJson(stats));
        }
        else
        {
            stats = JsonUtility.FromJson<Stats>(PlayerPrefs.GetString(keySats));
        }
    }

    public void MultiplayerAddWin(string name)
    {
        if (!stats.name.Contains(name))
        {
            stats.name.Add(name);
            stats.theWins.wins.Add(1);
        }
        else
        {
            stats.theWins.wins[stats.name.IndexOf(name)] += 1;
        }
        PlayerPrefs.SetString(keySats, JsonUtility.ToJson(stats));
    }

    public void SinglePlayerWin()
    {
        stats.singlePlayerWins++;
        PlayerPrefs.SetString(keySats, JsonUtility.ToJson(stats));
    }

    public void ComputerWin()
    {
        stats.compWins++;
        PlayerPrefs.SetString(keySats, JsonUtility.ToJson(stats));
    }

    [ContextMenu("Sort")]
    public void Sort()
    {
        for (int i = 0; i < stats.theWins.wins.Count; i++)
        {
            for (int j = 0; j < stats.theWins.wins.Count; j++)
            {
                if (i > j)
                {
                    if (stats.theWins.wins[i] > stats.theWins.wins[j])
                    {
                        int temp = stats.theWins.wins[i];
                        string temp1 = stats.name[i];
                        stats.theWins.wins[i] = stats.theWins.wins[j];
                        stats.name[i] = stats.name[j];
                        stats.theWins.wins[j] = temp;
                        stats.name[j] = temp1;
                    }
                }
                else
                {
                    if (stats.theWins.wins[i] < stats.theWins.wins[j])
                    {
                        int temp = stats.theWins.wins[i];
                        string temp1 = stats.name[i];
                        stats.theWins.wins[i] = stats.theWins.wins[j];
                        stats.name[i] = stats.name[j];
                        stats.theWins.wins[j] = temp;
                        stats.name[j] = temp1;
                    }
                }
            }
        }
    }

    public void SetAll()
    {
        if (stats.name.Count < 10)
        {
            for (int i = 0; i < stats.name.Count; i++)
            {
                multiplayerNames[i].transform.parent.parent.gameObject.SetActive(true);
                multiplayerNames[i].text = stats.name[i];
                multiplayerScores[i].text = stats.theWins.wins[i].ToString();
            }
            for (int i = stats.name.Count; i < 9; i++)
            {
                multiplayerNames[i].transform.parent.parent.gameObject.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < 9; i++)
            {
                multiplayerNames[i].transform.parent.parent.gameObject.SetActive(true);
                multiplayerNames[i].text = stats.name[i];
                multiplayerScores[i].text = stats.theWins.wins[i].ToString();
            }
        }
        singlePlayerScore.text = stats.singlePlayerWins.ToString();
        computerScore.text = stats.compWins.ToString();
    }


    public void OpenPVPStat()
    {
        ColorBlock colors = PVPButton.colors;
        ColorBlock colors1 = PVCButton.colors;
        colors.normalColor = Color.white;
        colors1.normalColor = Color.black;
        PVPButton.colors = colors;
        PVCButton.colors = colors1;
        pvpPanel.SetActive(true);
        pvcPanel.SetActive(false);
    }

    public void OpenPVCStat()
    {
        ColorBlock colors = PVPButton.colors;
        ColorBlock colors1 = PVCButton.colors;
        colors.normalColor = Color.black;
        colors1.normalColor = Color.white;
        PVPButton.colors = colors;
        PVCButton.colors = colors1;
        pvpPanel.SetActive(false);
        pvcPanel.SetActive(true);
    }
}

[Serializable]
public class Stats
{
    public List<string> name;
    public Wins theWins;
    public int singlePlayerWins, compWins;
}

[Serializable]
public class Wins
{
    public List<int> wins;
}
